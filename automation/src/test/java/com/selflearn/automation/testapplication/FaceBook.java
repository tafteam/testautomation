package com.selflearn.automation.testapplication;

import com.selflearn.automation.pageobjects.Application;
import com.selflearn.support.AbstractSeleniumTestCase;
import com.selflearn.support.dataprovider.ExcelData;

public class FaceBook extends AbstractSeleniumTestCase{

	@Override
	public String getExcelPath() {
		return System.getProperty("user.dir")+"/data/automation-suite/faceBook.xlsx";
	}
	
	@Override
	public void executeTest() throws Exception 
	{
		
		Application application = new Application(this, scenario.get());
		application.launchFaceBook()
		.loginToFaceBook();
	}

}
