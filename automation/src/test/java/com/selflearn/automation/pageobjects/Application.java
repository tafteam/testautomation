package com.selflearn.automation.pageobjects;

import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;

import org.slf4j.Logger;

import com.selflearn.support.PageDataProvider;
import com.selflearn.support.WebOperation;
import com.selflearn.support.dataprovider.ExcelData;
import com.selflearn.xsd.ObjectFactory;

public class Application {

	private WebOperation webOp;
	private ExcelData excelData;
	private Logger logger;
	
	public Application(WebOperation webOp, ExcelData excelData) throws Exception 
	{
		PageDataProvider.unmarshallPageData(ObjectFactory.class);
		this.webOp = webOp;
		this.excelData = excelData;
		logger = webOp.getLogger();
	}

	public LoginPage launchFaceBook() throws Exception{
		logger.info("Launching facebook.com");
		String url = "https://www.facebook.com";
		boolean isServicesUp=false;
		isServicesUp = checkApplicationIsUp(url);
		
		if (isServicesUp)
		{
			System.out.println("Application is up and runing successful");
			webOp.getURL(url);
			return new LoginPage(webOp,excelData);
		}
		else 
		{
			System.out.println("Application is down");
			throw new RuntimeException("Services " + url + "is down, cannot initiate tests");
		}
	}

	public boolean checkApplicationIsUp(String url) throws Exception 
	{
		/*Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("proxy.intra.bt.com", 8080));
		HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(url).openConnection(proxy);
		//HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
		httpURLConnection.setRequestMethod("GET");
		httpURLConnection.connect();
		if (HttpURLConnection.HTTP_OK == httpURLConnection.getResponseCode())
		{
			return true;
		}
		else 
		{
			return false;
		}*/
		return true;
	}
}
