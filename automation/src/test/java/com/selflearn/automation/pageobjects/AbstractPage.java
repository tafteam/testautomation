package com.selflearn.automation.pageobjects;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.slf4j.Logger;

import com.selflearn.support.PageDataProvider;
import com.selflearn.support.WebOperation;
import com.selflearn.support.dataprovider.ExcelData;
import com.selflearn.support.util.LocatorUtil;
import com.selflearn.xsd.ObjectFactory;
import com.sun.jna.platform.win32.Tlhelp32;


public abstract class AbstractPage {

	protected WebOperation webOp;
	public static Map<Class<? extends AbstractPage>, Map<String, String>> allLocators = new HashMap<Class<? extends AbstractPage>, Map<String,String>>();
	public Map<String, String> locator;
	public ExcelData excelData;
	public Logger logger;

	public AbstractPage(WebOperation webOp, ExcelData excelData) throws Exception {
		PageDataProvider.unmarshallPageData(ObjectFactory.class);
		this.webOp = webOp;
		this.excelData = excelData;
		logger = webOp.getLogger();
		loadPageLocators();
	}

	public void loadPageLocators()
	{
		if(!allLocators.containsKey(getClass()))
		{
			allLocators.put(getClass(), loadLocatorForPage(getClass()));
		}
		locator = allLocators.get(getClass());
	}


	public Map<String, String> loadLocatorForPage(Class<? extends AbstractPage> clazz)
	{
		try
		{
			Map<String, String> locator = new HashMap<String, String>();
			String propertyFile = clazz.getName().replaceAll(Pattern.quote("."), "/") + ".properties";
			InputStream inputStream = ClassLoader.getSystemResourceAsStream(propertyFile);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			String line=null;
			while ((line=bufferedReader.readLine()) != null)
			{
				String key = line.trim().substring(0, line.trim().indexOf('=')).trim();
				String value = line.trim().substring(line.trim().indexOf('=') + 1);
				locator.put(key, value);
			}
			return locator;
		}
		catch (Exception e)
		{
			System.out.println("Could not load the locators for the page " + getClass());
		}
		return null;
	}

	public By getElement(String elementName, String... params)
	{
		String propertyValue = locator.get(elementName);
		String locatorType = propertyValue.substring(0, propertyValue.indexOf("|")).trim();
		String locatorValue = propertyValue.substring(propertyValue.indexOf("|") + 1).trim();
		locatorValue = getDynamicLocatorValue(locatorValue, params);
		return LocatorUtil.getLocator(elementName, locatorType, locatorValue);
	}

	private String getDynamicLocatorValue(String locatorValue, String... params)
	{
		if (params != null)
		{
			for (int i=0; i < params.length; i ++)
			{
				locatorValue = locatorValue.replaceAll(Pattern.quote("%" + (i + 1)), params[i]);
			}
		}
		return locatorValue;
	}
}
