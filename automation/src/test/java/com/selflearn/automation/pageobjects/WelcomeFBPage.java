package com.selflearn.automation.pageobjects;

import com.selflearn.support.WebOperation;
import com.selflearn.support.dataprovider.ExcelData;

public class WelcomeFBPage extends AbstractPage {

	public WelcomeFBPage(WebOperation webOp,ExcelData excelData) throws Exception {
		super(webOp, excelData);
	}
	
	
	public LoginPage logout() throws Exception {
		webOp.clickElement(getElement("logoutMenu dropdown"));
		Thread.sleep(2000);
		webOp.clickElement(getElement("Log out link"));
		return new LoginPage(webOp, excelData);
	}
}
