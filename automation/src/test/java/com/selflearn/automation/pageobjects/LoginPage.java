package com.selflearn.automation.pageobjects;

import org.openqa.selenium.By;

import com.selflearn.support.PageDataProvider;
import com.selflearn.support.WebOperation;
import com.selflearn.support.dataprovider.ExcelData;
import com.selflearn.xsd.Login;

public class LoginPage extends AbstractPage {

	public LoginPage(WebOperation webOp,ExcelData excelData) throws Exception {
		super(webOp,excelData);
	}

	public WelcomeFBPage loginToFaceBook() throws Exception
	{
		Login login = PageDataProvider.getPageData(excelData.getThisScenario().get("login"));
		
		webOp.setTextOnElement(getElement("Email or Phone textField"), login.getUserName());
		webOp.setTextOnElement(getElement("Password textField"), login.getPassword());
		webOp.clickElement(getElement("Log In button"));
		webOp.waitForElementOnPage(By.cssSelector("a[href*='edit_profile']"));
		return new WelcomeFBPage(webOp,excelData);
	}
}
