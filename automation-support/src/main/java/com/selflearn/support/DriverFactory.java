package com.selflearn.support;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DriverFactory {
	DesiredCapabilities desiredCapabilities;
	
	public DriverFactory(DesiredCapabilities capability)
	{
		desiredCapabilities = capability;
	}
	
	public DriverFactory()
	{
	}
	
	protected WebDriver getDriverInstance() throws IOException 
	{
		WebDriver driver;
		
		if ("ie".equalsIgnoreCase(System.getProperty("browser"))) {
			System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"externalResource/IEDriverServer.exe");
			return new InternetExplorerDriver();
		}
		else {
			FirefoxProfile profile = new FirefoxProfile();
			FirefoxBinary ffBinary = new FirefoxBinary(new File(System.getProperty("user.dir")+"/externalResource/firefox_28/firefox.exe"));
			return new FirefoxDriver(ffBinary, profile);
		}
	}
	
}
