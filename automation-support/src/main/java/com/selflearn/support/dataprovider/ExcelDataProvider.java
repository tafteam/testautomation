package com.selflearn.support.dataprovider;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataProvider {

	//Need to add the constructor to set the excel file and sheet to be read in that file


	public ExcelData[] readExcel(String excelPath)  {
		File excelFile = new File(excelPath);
		List<Map<String, String>> tcList = new ArrayList<Map<String, String>>();
		Set<ExcelData> excelDataSet = new LinkedHashSet<ExcelData>();
		try 
		{
			XSSFWorkbook workbook = new XSSFWorkbook(excelFile.getPath());
			XSSFSheet sheet = workbook.getSheet("provide");
			XSSFRow row = null;
			int numberOfRows = sheet.getLastRowNum();
			String key=null;

			for (int rowIndex=0; rowIndex<=numberOfRows; rowIndex++)
			{
				row = sheet.getRow(rowIndex);
				if(row==null){
					continue;
				}
				String tempKey = getValue((XSSFCell)row.getCell(0));
				if (tempKey!=null && !tempKey.trim().isEmpty()){
					key=tempKey;
				}
				for (int columnIndex=1; columnIndex<row.getLastCellNum(); columnIndex++) 
				{
					String value = getValue((XSSFCell)row.getCell(columnIndex));
					if (value!=null) {
						value = value.trim();
					}
					if (tcList.size()<columnIndex) 
					{
						tcList.add(new LinkedHashMap<String, String>());
					}
					if (!tcList.get(columnIndex-1).containsKey(key))
					{
						tcList.get(columnIndex-1).put(key, value);
					}
				}
			}

			for (Map<String, String> map : tcList) 
			{
				if (map.isEmpty()) 
				{
					continue; //discard this test
				}
				ExcelData ed = new ExcelData(map);
				if (ed.getId()==null || ed.getId().trim().isEmpty())
				{
					continue;
				}
				//Check for duplicate tests
				if(excelDataSet.contains(ed)) 
				{
					throw new RuntimeException("Contains duplicate tests with id : "+ ed.getId());
				}
				excelDataSet.add(ed);
			}
			return excelDataSet.toArray(new ExcelData[]{});

		}
		catch (IOException e) 
		{
			e.printStackTrace();
			return null;
		}

	}

	public static String getValue(XSSFCell cell) 
	{
		if (cell==null)
			return null;

		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) 
			return cell.getRawValue();

		else
			return cell.getStringCellValue();
	}


}
