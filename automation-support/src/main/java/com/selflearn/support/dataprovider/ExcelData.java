package com.selflearn.support.dataprovider;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ExcelData {
	
	private static final String ID = "$ID$";
	private static final String DESCRIPTION = "$DESCRIPTION$";
	private static final String SKIP = "$SKIP$";
	private static final String SUITE = "$SUITE$";
	
	
	
	private String id;
	private boolean skip;
	private String description;
	private String suite;
	

	private Map<String, String> thisScenario;
	
	public ExcelData(Map<String, String> map) 
	{
		setThisScenario(map);
		this.id=thisScenario.remove(ID);
		this.description=thisScenario.remove(DESCRIPTION);
		String skipText = thisScenario.remove(SKIP).toString();
		if (skipText!=null && !skipText.trim().isEmpty())
		{
		this.skip = "y".equalsIgnoreCase(skipText) || "yes".equalsIgnoreCase(skipText) || "true".equalsIgnoreCase(skipText);
		}
		else 
		{
			this.skip=false;
		}
	}
	
	public String getId() {
		return id;
	}

	public boolean isSkip() {
		return skip;
	}


	public String getDescription() {
		return description;
	}

	public Map<String, String> getThisScenario() {
		return thisScenario;
	}

	public void setThisScenario(Map<String, String> thisScenario) {
		this.thisScenario = thisScenario;
	}
	
	@Override
	public String toString()
	{
		return description==null? id : id+"("+description+")";
	}
	
	@Override
	public int hashCode()
	{
		return id.hashCode();
	}
	
	@Override
	public boolean equals(Object other) 
	{
		if(!(other instanceof ExcelData))
		{
			return false;
		}
		
		ExcelData otherData = (ExcelData)other;
		
		return id.equals(otherData.id);
	}
}
