package com.selflearn.support.util;

import org.openqa.selenium.By;

public class LocatorUtil
{
    public static By getLocator(String name, String type, String locatorVal)
    {
        if ("ClassName".equalsIgnoreCase(type))
        {
            return new ByClassName(name, locatorVal);
        }
        else if ("CssSelector".equalsIgnoreCase(type))
        {
            return new ByCssSelector(name, locatorVal);
        }
        else if ("Id".equalsIgnoreCase(type))
        {
            return new ById(name, locatorVal);
        }
        else if ("LinkText".equalsIgnoreCase(type))
        {
            return new ByLinkText(name, locatorVal);
        }
        else if ("Name".equalsIgnoreCase(type))
        {
            return new ByName(name, locatorVal);
        }
        else if ("PartialLinkText".equalsIgnoreCase(type))
        {
            return new ByPartialLinkText(name, locatorVal);
        }
        else if ("TagName".equalsIgnoreCase(type))
        {
            return new ByTagName(name, locatorVal);
        }
        else if ("XPath".equalsIgnoreCase(type))
        {
            return new ByXPath(name, locatorVal);
        }
        else
        {
            return null;
        }
    }
}

class ByClassName extends org.openqa.selenium.By.ByClassName
{

    private String	name;
    private String	className;

    public ByClassName(String name, String className)
    {
        super(className);
        this.name = name;
        this.className = className;
    }

    @Override
    public String toString()
    {
        return name + " with class '" + className + "'";
    }
}

class ByCssSelector extends org.openqa.selenium.By.ByCssSelector
{
    private String	name;
    private String	cssSelector;

    public ByCssSelector(String name, String cssSelector)
    {
        super(cssSelector);
        this.name = name;
        this.cssSelector = cssSelector;
    }

    @Override
    public String toString()
    {
        return name + " with css selector '" + cssSelector + "'";
    }
}

class ById extends org.openqa.selenium.By.ById
{

    private String	name;
    private String	id;

    public ById(String name, String id)
    {
        super(id);
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString()
    {
        return name + " with id '" + id + "'";
    }
}

class ByLinkText extends org.openqa.selenium.By.ByLinkText
{
    private String	name;
    private String	linkText;

    public ByLinkText(String name, String linkText)
    {
        super(linkText);
        this.name = name;
        this.linkText = linkText;
    }

    @Override
    public String toString()
    {
        return name + " with link text '" + linkText + "'";
    }
}

class ByName extends org.openqa.selenium.By.ByName
{
    private String	name;
    private String	inputName;

    public ByName(String name, String inputName)
    {
        super(inputName);
        this.name = name;
        this.inputName = inputName;
    }

    @Override
    public String toString()
    {
        return name + " with name '" + inputName + "'";
    }
}

class ByPartialLinkText extends org.openqa.selenium.By.ByPartialLinkText
{
    private String	name;
    private String	partialLinkText;

    public ByPartialLinkText(String name, String partialLinkText)
    {
        super(partialLinkText);
        this.name = name;
        this.partialLinkText = partialLinkText;
    }

    @Override
    public String toString()
    {
        return name + " with partial link text '" + partialLinkText + "'";
    }
}

class ByTagName extends org.openqa.selenium.By.ByTagName
{
    private String	name;
    private String	tagName;

    public ByTagName(String name, String tagName)
    {
        super(tagName);
        this.name = name;
        this.tagName = tagName;
    }

    @Override
    public String toString()
    {
        return name + " with tag name '" + tagName + "'";
    }
}

class ByXPath extends org.openqa.selenium.By.ByXPath
{
    private String	name;
    private String	xPath;

    public ByXPath(String name, String xPath)
    {
        super(xPath);
        this.name = name;
        this.xPath = xPath;
    }

    @Override
    public String toString()
    {
        return name + " with XPath '" + xPath + "'";
    }

}