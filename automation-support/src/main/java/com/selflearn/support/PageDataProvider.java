package com.selflearn.support;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;


public class PageDataProvider {

	public static Map<String, Object> dataMap;
	
	public static <T> T getPageData(String key)
	{
		return (T) dataMap.get(key);
	}
	
	public static void unmarshallPageData(Class<?> clazz) throws Exception
	{
		if (dataMap!=null)
			return;
		
		dataMap = new HashMap<String, Object>();
		String pageDataPath = System.getProperty("user.dir")+"/data/PageData.xml";
		JAXBContext jc = JAXBContext.newInstance(clazz);
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		Object data = unmarshaller.unmarshal(new File(pageDataPath));
		
		for (Field field : data.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value = field.get(data);
            if (value instanceof List<?>) {
                List<?> list = (List<?>) value;
                for (Object pageData : list) {
                	Field idField = pageData.getClass().getDeclaredField("id");
                    idField.setAccessible(true);
                    String id = (String) idField.get(pageData);
                    dataMap.put(id, pageData);
                }
            }
        }
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
