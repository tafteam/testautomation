package com.selflearn.support;

import com.google.common.io.Files;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;

import java.io.*;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by sjn on 6/27/2015.
 */
public class Screenshot
{
    private int shotCount=0;
    FileOutputStream fileOutputStream = null;
    ZipOutputStream zipOutputStream = null;
    FileInputStream fileInputStream = null;

    WebDriver driver=null;

    public Screenshot(WebDriver driver, Class clazz, String fileName) throws IOException {
        this.driver=driver;
        String className = clazz.getName().replaceAll(Pattern.quote("."), "/");
        System.out.println(System.getProperty("user.dir")+"/screenshot/"+className+"/"+fileName+new Date().toString()+".zip");
        File file = new File(System.getProperty("user.dir")+"/screenshot/"+className+"/"+fileName+".zip");
        Files.createParentDirs(file);
        if (file.createNewFile()) {
            System.out.println("File created successfully");
        }
        else {

        }
        fileOutputStream = new FileOutputStream(file);
        zipOutputStream = new ZipOutputStream(new BufferedOutputStream(fileOutputStream));
    }

    public void takeScreenshotAndZip() throws IOException {
        File input = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        fileInputStream = new FileInputStream(input);
        ZipEntry ze = new ZipEntry(shotCount+".png");
        System.out.println("Zipping the file: " + input.getName());
        zipOutputStream.putNextEntry(ze);
        byte[] tmp = new byte[4*1024];
        int size = 0;
        while((size = fileInputStream.read(tmp)) != -1){
            zipOutputStream.write(tmp, 0, size);
        }
        shotCount++;

    }

    public void saveToPhysicalFile() throws IOException {
        zipOutputStream.flush();
    }

    public void close() throws IOException {
        fileInputStream.close();
        zipOutputStream.close();
    }
}
