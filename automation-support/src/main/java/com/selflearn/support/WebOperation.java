package com.selflearn.support;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;

public interface WebOperation {
	
	void clickElement(By by);
	
	void clickElementJavascript(By by);
	
	void selectByVisibleText(By by);
	
	WebDriver getDriver();
	
	void getURL(String url);

	void setTextOnElement(By by, String textToSet);
	
	Logger getLogger();
	
	void waitTillAllCallsDone(Integer timeOut);
	
	boolean waitForElementOnPage(By by);
}
