package com.selflearn.support;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.uncommons.reportng.HTMLReporter;

import com.selflearn.support.dataprovider.ExcelData;
import com.selflearn.support.dataprovider.ExcelDataProvider;

@Listeners(HTMLReporter.class)
public abstract class AbstractSeleniumTestCase implements WebOperation {

	protected ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
	protected ThreadLocal<ExcelData> scenario = new ThreadLocal<ExcelData>();
	protected ThreadLocal<Screenshot> screehshot = new ThreadLocal<Screenshot>();
	protected Logger logger = null;
	protected AtomicInteger counter = new AtomicInteger(1);
	protected Integer timeOut=45;

	public abstract void executeTest() throws Exception;

	public abstract String getExcelPath();

	public WebDriver getDriver() {
		return driver.get();
	}

	public void getURL(String url) {
		driver.get().get(url);
	}

	@BeforeMethod
	public void beforeTest(ITestContext context) throws Exception {
		startLogger(context);
		logger.info("Fullfilling before test");
		logger.info("Launching the browser");
		driver.set(new DriverFactory().getDriverInstance());
		driver.get().manage().window().maximize();
		driver.get().manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		driver.get().manage().timeouts().pageLoadTimeout(timeOut, TimeUnit.SECONDS);
	}

	private synchronized void startLogger(ITestContext context) {
		
		logger = LoggerFactory.getLogger(getClass());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HmsS");
		String fileName = getClass().getSimpleName() + sdf.format(new Date());
		System.out.println(fileName);
		//MDC.put("scenarioFile",	getClass().getSimpleName() + counter.getAndIncrement());
		MDC.put("scenarioFile",	fileName);
		context.setAttribute("logFileName", fileName);
	}

	@Test(dataProvider = "dataProvider")
	public void seleniumTestCase(ExcelData excelData) throws Throwable {
		try {
			logger.info("Starting the test!!!");
			scenario.set(excelData);
			initializeScreenshot(scenario.get().getId());
			executeTest();
		} catch (NullPointerException npe) {
			System.out.println(npe.getMessage());
			npe.printStackTrace();
			throw npe;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} catch (Throwable t) {
			t.printStackTrace();
			throw t;
		} 
	}

	@DataProvider(name = "dataProvider", parallel = true)
	public Object[][] dataProvider(ITestContext iTestContext) {
		// logger.info("Inside dataProvide Method of " + getClass().getName());
		ExcelDataProvider excelDataProvider = new ExcelDataProvider();
		ExcelData[] excelDataArray = excelDataProvider
				.readExcel(getExcelPath());
		System.out.println("before skip " + excelDataArray.length);
		excelDataArray = filterSkippedScenarios(excelDataArray);
		Object[][] excelObject = new Object[excelDataArray.length][1];
		for (int index = 0; index < excelDataArray.length; index++) {
			excelObject[index][0] = excelDataArray[index];
		}
		return excelObject;
	}

	@AfterMethod
	public void afterTest() throws IOException {
		System.out.println("Quitting all the driver");
		driver.get().quit();
		screehshot.get().saveToPhysicalFile();
		screehshot.get().close();
	}

	public ExcelData[] filterSkippedScenarios(ExcelData[] ed) {
		List<ExcelData> excelDataList = new ArrayList<ExcelData>();
		for (ExcelData data : ed) {
			if (!data.isSkip())
				excelDataList.add(data);
		}
		return excelDataList.toArray(new ExcelData[] {});
	}

	@Override
	public void clickElement(By by) {
		if (isTakeScreenshot()) {
            try {
                screehshot.get().takeScreenshotAndZip();
            }
            catch(Exception e) {
                System.out.println("Could not take screenshot as of now :(");
            }
        }		
		WebElement element = new WebDriverWait(getDriver(), 180)
				.until(ExpectedConditions.elementToBeClickable(by));
		focousAndClick(by, element);

	}

	public void focousAndClick(By by, WebElement element) {
		logger.info("Clicking on element " + by.toString());
		synchronized (AbstractSeleniumTestCase.class) {
			getDriver().switchTo().defaultContent();
			((JavascriptExecutor) getDriver()).executeScript("window.focus()");
			element.click();
		}
	}

	@Override
	public void clickElementJavascript(By by) {

	}

	@Override
	public void setTextOnElement(By by, String textToSet) {
		WebElement element = new WebDriverWait(getDriver(), 180)
				.until(ExpectedConditions.elementToBeClickable(by));
		logger.info("Setting text " + textToSet + " on element "
				+ by.toString());
		element.sendKeys(textToSet);
	}

	@Override
	public void selectByVisibleText(By by) {
		// TODO Auto-generated method stub
	}

	public Logger getLogger() {
		return logger;
	}

	@Override
	public void waitTillAllCallsDone(Integer timeOut) {
		JavascriptExecutor jse = (JavascriptExecutor) driver.get();
		while(timeOut !=0) { 
			if (!((Boolean)jse.executeScript("return window.loaded"))) {
				return;
			}
			else {
				timeOut--;
				try {
					Thread.sleep(1000);
				} 
				catch (InterruptedException e) {
					
				}
			}
			
		}
	}
	
	public boolean waitForElementOnPage(By by) {
		new WebDriverWait(getDriver(), timeOut).until(ExpectedConditions.visibilityOf(getDriver().findElement(by)));
		try {
			return ( getDriver().findElement(by).isDisplayed() && getDriver().findElement(by).isEnabled() ) ? true : false;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	protected synchronized void initializeScreenshot(String fileName) throws IOException {
		logger.info("This is the scenario ID : " + fileName);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HmsS");
		fileName = fileName + "_"+sdf.format(new Date());
        if (isTakeScreenshot())
        {
            screehshot.set(new Screenshot(getDriver(), getClass(), fileName));
        }
    }
	
	boolean isTakeScreenshot() {
        return "yes".equalsIgnoreCase(System.getProperty("take.screenshot"));
    }

	
	
}
