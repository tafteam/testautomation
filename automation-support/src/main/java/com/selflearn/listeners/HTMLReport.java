package com.selflearn.listeners;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import org.apache.commons.io.FileUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.testng.ISuite;
import org.testng.xml.XmlSuite;
import org.uncommons.reportng.HTMLReporter;
import org.uncommons.reportng.ReportNGException;
import org.uncommons.reportng.ReportNGUtils;
//import com.bt.helpdeskautomation.support.utils.BTReportingUtils;

public class HTMLReport extends HTMLReporter
{
	private static final String			ENCODING			= "UTF-8";
	private static final String			TEMPLATES_PATH		= "org/uncommons/reportng/templates/html/";
	private static final String			BT_TEMPLATES_PATH	= "com/bt/helpdeskautomation/support/templates/";
	private static final String			BT_RESOURCE_PATH	= "/com/bt/helpdeskautomation/support/resources/";
	private static final String			META_KEY			= "meta";
	private static final String			UTILS_KEY			= "utils";
	//private static final ReportNGUtils	UTILS				= new BTReportingUtils();
	private static final String			MESSAGES_KEY		= "messages";
	private static final ResourceBundle	MESSAGES			= ResourceBundle.getBundle("org.uncommons.reportng.messages.reportng", META.getLocale());

	private static Map<String, String>	replacements		= new HashMap<String, String>();

	static
	{
		replacements.put("org/uncommons/reportng/templates/html/results.html" + TEMPLATE_EXTENSION, BT_TEMPLATES_PATH
				+ "results.html" + TEMPLATE_EXTENSION);
	}

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
			String outputDirectoryName)
	{
		super.generateReport(xmlSuites, suites, outputDirectoryName);
		try
		{
			copyResources(outputDirectoryName);
		}
		catch (IOException e)
		{
			throw new ReportNGException("Failed generating HTML report.", e);
		}
	}

	private void copyResources(String targetDirectoryName) throws IOException
	{
		File outputDirectory = new File(targetDirectoryName, "html");
		copyResourceToFile(outputDirectory, "leftArrow.png");
		copyResourceToFile(outputDirectory, "logIcon.png");
		copyResourceToFile(outputDirectory, "zipIcon.png");
		copyResourceToFile(outputDirectory, "fiddlerIcon.png");

		File screenShots = new File("screenshots");
		if (screenShots.exists() && screenShots.isDirectory())
		{
			FileUtils.copyDirectory(screenShots, outputDirectory);
		}

		File logs = new File("logs");
		if (logs.exists() && logs.isDirectory())
		{
			FileUtils.copyDirectory(logs, outputDirectory);
		}

		File fiddlers = new File("fiddlers");
		if (fiddlers.exists() && fiddlers.isDirectory())
		{
			FileUtils.copyDirectory(fiddlers, outputDirectory);
		}
	}

	private void copyResourceToFile(File outputDirectory, String resource) throws IOException
	{
		String resourceName = BT_RESOURCE_PATH + resource;
		InputStream inputStream = getClass().getResourceAsStream(resourceName);
		FileUtils.copyInputStreamToFile(inputStream, new File(outputDirectory, resource));
	}

	@Override
	protected void generateFile(File file, String templateName, VelocityContext context)
			throws Exception
	{
		Writer writer = new BufferedWriter(new FileWriter(file));
		try
		{
			Velocity.mergeTemplate(getReplacement(TEMPLATES_PATH + templateName), ENCODING, context, writer);
			writer.flush();
		}
		finally
		{
			writer.close();
		}
	}

	@Override
	protected VelocityContext createContext()
	{
		VelocityContext context = new VelocityContext();
		context.put(META_KEY, META);
		//context.put(UTILS_KEY, UTILS);
		context.put(MESSAGES_KEY, MESSAGES);
		return context;
	}

	private String getReplacement(String replacementTemplate)
	{
		return replacements.containsKey(replacementTemplate) ? replacements.get(replacementTemplate)
				: replacementTemplate;
	}
}
