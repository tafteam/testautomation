Test Automation Framework(TAF) is a highly reliable, scalable and free automation framework that can be used by small, medium and large organizations to effectively manage and run their automation suites

The framework is extremely lightweight and can easily integrate with external build systems to provide  a continuous delivery platform.
It is a state of the art design using selenium and java

Salient Features:

* Maven Based: Makes the fwk lightweight, easier dependency management. Easy integration with continuous build system 
* TestNg Based: Easier test management. Can have multiple suits eg: Sanity Regression, etc. Ability to run TCs at all granularity levels
* Modular Design: Extremely modular and extensible. 
* Pageobject: Each page of the application is modeled as a page object for code maintainability and faster test case creation
* Locator decoupling with code to prevent test code rewriting.
* Parallel: The architecture supports parallel execution of independent tests, thereby reducing the turnaround time and increasing ROI 
* Logging: Excellent logging for application debugging in case of failures. Provides support for screenshots and console logs 
* Multi Browser Support: Ability to run on multiple browsers from configuration parameter
* Test Case Execution Control: Ability to configure to run single tests,suites or different versions of regression suites.
* Reporting - standard reporting, we can enhance further
* Notification: Can be easily configured to send notifications for test failures and detailed test reports to test managers
* Tracking: Real time status of running tests. Ability to monitor the running tests through a vncsession (TBD)


Future Work:
Logs
All screenshots
Fiddler logs
Integration with SauceLabs
DIfferent browsers
Retry for each test case if needed
vncserver on suit start.

Validations:
Page level validations
Scenario level validations






# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This repository is an effort to build a generic test automation framework to test any software in web/mobile.

As of now, this has a capability to test web applications seamlessly.

 
* Version
This we can call V1.0. Going forward will include appium in place to test mobile application whether native app or mobile web browser.


* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact